﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using MovieRama.Web.Entities;

namespace MovieRama.Web.Models
{
    public class MovieViewModel
    {
        public int MovieId { get; set; }

        [Required] [StringLength(80)] public string Title { get; set; }

        [Required] [StringLength(500)] public string Description { get; set; }

        public DateTime DateOfPublication { get; set; }

        public bool ShowEditLink { get; set; }

        public ApplicationUser PostedByApplicationUser { get; set; }

        public int Likes { get; set; }
        public int Hates { get; set; }

        public bool UserAlreadyLiked { get; set; }
        public bool UserAlreadyHated { get; set; }

        public MovieViewModel()
        {
        }

        public MovieViewModel(Movie movie)
        {
            MovieId = movie.MovieID;
            DateOfPublication = movie.DateOfPublication;
            Description = movie.Description;
            Title = movie.Title;
        }

        public MovieViewModel(Movie movie, bool showEditLink, string userId)
        {
            MovieId = movie.MovieID;
            DateOfPublication = movie.DateOfPublication;
            Description = movie.Description;
            Title = movie.Title;
            ShowEditLink = showEditLink;
            PostedByApplicationUser = movie.User;

            if (movie.MovieUserRatings == null) return;
            Likes = movie.MovieUserRatings.Count(m => m.RatingId == (int) RatingTypes.Like);
            Hates = movie.MovieUserRatings.Count(m => m.RatingId == (int)RatingTypes.Hate);

            if (userId == null) return;
            UserAlreadyLiked = movie.MovieUserRatings.Any(x => x.UserId == userId && x.RatingId == (int)RatingTypes.Like);
            UserAlreadyHated = movie.MovieUserRatings.Any(x => x.UserId == userId && x.RatingId == (int)RatingTypes.Hate);
        }
    }
}
