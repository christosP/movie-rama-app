﻿using System.Collections.Generic;

namespace MovieRama.Web.Models
{
    public class MoviesBostedByUserViewModel
    {
        public IEnumerable<MovieViewModel> MovieViewModels { get; set; }
        public ApplicationUser PostedByApplicationUser { get; set; }
    }
}
