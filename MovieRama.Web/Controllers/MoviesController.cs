﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MovieRama.Web.Domain;
using MovieRama.Web.Entities;
using MovieRama.Web.Models;

namespace MovieRama.Web.Controllers
{
    public class MoviesController : Controller
    {
        private readonly IMoviesRepository _moviesRepository;
        private string CurrentUserId => User.FindFirstValue(ClaimTypes.NameIdentifier);

        public MoviesController(IMoviesRepository moviesRepository)
        {
            _moviesRepository = moviesRepository;
        }

        // GET: Movies
        public async Task<IActionResult> Index(string sortOrder)
        {
            var movies = await _moviesRepository.GetMoviesOrderedByAsync(sortOrder);

            var moviesViewModels = movies.Select(movie =>
                new MovieViewModel(movie, movie.UserId == CurrentUserId, CurrentUserId));

            return View(moviesViewModels);
        }

        // GET: Movies/Create
        [Authorize]
        public IActionResult Create()
        {
            return View();
        }

        // POST: Movies/Create
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Title", "Description")] MovieViewModel movieViewModel)
        {
            if (ModelState.IsValid)
            {
                var movie = new Movie
                {
                    Title = movieViewModel.Title,
                    Description = movieViewModel.Description,
                    DateOfPublication = DateTime.Now,
                    UserId = CurrentUserId
                };

                await _moviesRepository.InsertMovieAsync(movie);

                return RedirectToAction(nameof(Index));
            }

            return View(movieViewModel);
        }

        // GET: Movies/Edit/5
        [Authorize]
        public async Task<IActionResult> Edit(int id)
        {
            var movie = await _moviesRepository.FindMovieByIdAsync(id);

            if (movie == null)
            {
                return NotFound();
            }

            if (movie.HasNotBeenCreatedByUser(CurrentUserId))
            {
                return Unauthorized();
            }

            return View(new MovieViewModel(movie));
        }

        // POST: Movies/Edit/5
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("MovieId", "Title", "Description")]
            MovieViewModel movieViewModel)
        {
            if (id != movieViewModel.MovieId)
            {
                return NotFound();
            }

            var movie = await _moviesRepository.FindMovieByIdAsync(id);

            if (movie.HasNotBeenCreatedByUser(CurrentUserId))
            {
                return Unauthorized();
            }

            if (ModelState.IsValid)
            {
                movie.Title = movieViewModel.Title;
                movie.Description = movieViewModel.Description;

                await _moviesRepository.UpdateMovieAsync(movie);

                return RedirectToAction(nameof(Index));
            }

            return View(new MovieViewModel(movie));
        }

        [HttpGet]
        public async Task<IActionResult> PostedByUser(string id, string sortOrder)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return NotFound();
            }

            var moviesPostedByUser = await _moviesRepository.GetMoviesPostedByUserOrderedByAsync(id, sortOrder);

            if (moviesPostedByUser.Count == 0)
            {
                return NotFound();
            }

            bool ShowEditLink(Movie movie) => User.Identity.IsAuthenticated
                                              && string.Equals(CurrentUserId, movie.UserId,
                                                  StringComparison.InvariantCultureIgnoreCase);

            var movieViewModels = moviesPostedByUser.Select(movie =>
                    new MovieViewModel(movie, showEditLink: ShowEditLink(movie), userId: CurrentUserId))
                .ToList();

            var moviesPostedByUserViewModel = new MoviesBostedByUserViewModel
            {
                MovieViewModels = movieViewModels,
                PostedByApplicationUser = movieViewModels.First().PostedByApplicationUser
            };

            return View(moviesPostedByUserViewModel);
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
