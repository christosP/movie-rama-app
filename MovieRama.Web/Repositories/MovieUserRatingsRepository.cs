﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MovieRama.Web.Data;
using MovieRama.Web.Domain;
using MovieRama.Web.Entities;

namespace MovieRama.Web.Repositories
{
    public class MovieUserRatingsRepository : IMovieUserRatingsRepository
    {
        private readonly MovieRamaDbContext _context;

        public MovieUserRatingsRepository(MovieRamaDbContext context) => _context = context;

        public async Task<MovieUserRating> FindMovieUserRatingByMovieIdAndUserIdAsync(string userId, int movieId)
        {
            return await _context.MovieUserRatings.FirstOrDefaultAsync(mur => mur.UserId == userId && mur.MovieId == movieId);
        }

        public async Task<int> InsertMovieUserRatingAsync(MovieUserRating movieUserRating)
        {
            _context.MovieUserRatings.Add(movieUserRating);

            return await _context.SaveChangesAsync();
        }

        public async Task<int> DeleteMovieUserRatingAsync(MovieUserRating movieUserRating)
        {
            _context.MovieUserRatings.Remove(movieUserRating);
            return await _context.SaveChangesAsync();
        }

        public async Task<int> CountMovieLikesAsync(int movieId)
        {
            return await _context.MovieUserRatings.CountAsync(
                mur => mur.MovieId == movieId
                       && mur.RatingId == (int) RatingTypes.Like);
        }

        public async Task<int> CountMovieHatesAsync(int movieId)
        {
            return await _context.MovieUserRatings.CountAsync(
                mur => mur.MovieId == movieId
                       && mur.RatingId == (int) RatingTypes.Hate);
        }
    }
}