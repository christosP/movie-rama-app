﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MovieRama.Web.Data;
using MovieRama.Web.Domain;
using MovieRama.Web.Entities;

namespace MovieRama.Web.Repositories
{

    public class MoviesRepository : IMoviesRepository
    {
        private readonly MovieRamaDbContext _context;

        public MoviesRepository(MovieRamaDbContext context) => _context = context;

        public async Task<List<Movie>> GetMoviesOrderedByAsync(string orderBy)
        {
            List<Movie> movies;

            switch (orderBy)
            {
                case "likes":
                    movies = await _context.Movies
                        .Include(movie => movie.User)
                        .Include(movie => movie.MovieUserRatings)
                        .OrderByDescending(movie => movie.MovieUserRatings.Count(mur => mur.RatingId == (int)RatingTypes.Like))
                        .ToListAsync();
                    break;
                case "hates":
                    movies = await _context.Movies
                        .Include(movie => movie.User)
                        .Include(movie => movie.MovieUserRatings)
                        .OrderByDescending(movie => movie.MovieUserRatings.Count(mur => mur.RatingId == (int)RatingTypes.Hate))
                        .ToListAsync();
                    break;
                case "date-posted":
                    movies = await _context.Movies
                        .Include(movie => movie.User)
                        .Include(movie => movie.MovieUserRatings)
                        .OrderByDescending(movie=>movie.DateOfPublication)
                        .ToListAsync();
                    break;
                default:
                    movies = await _context.Movies
                        .Include(movie => movie.User)
                        .Include(movie => movie.MovieUserRatings)
                        .ToListAsync();
                    break;

            }

            return movies;
        }

        public async Task<List<Movie>> GetMoviesPostedByUserOrderedByAsync(string userId, string orderBy)
        {
            List<Movie> movies;

            switch (orderBy)
            {
                case "likes":
                    movies = await _context.Movies
                        .Include(movie => movie.User)
                        .Include(movie => movie.MovieUserRatings)
                        .Where(movie =>
                            string.Equals(movie.User.Id, userId, StringComparison.InvariantCultureIgnoreCase))
                        .OrderByDescending(movie =>
                            movie.MovieUserRatings.Count(mur => mur.RatingId == (int) RatingTypes.Like))
                        .ToListAsync();
                    break;
                case "hates":
                    movies = await _context.Movies
                        .Include(movie => movie.User)
                        .Include(movie => movie.MovieUserRatings)
                        .Where(movie =>
                            string.Equals(movie.User.Id, userId, StringComparison.InvariantCultureIgnoreCase))
                        .OrderByDescending(movie =>
                            movie.MovieUserRatings.Count(mur => mur.RatingId == (int) RatingTypes.Hate))
                        .ToListAsync();
                    break;
                case "date-posted":
                    movies = await _context.Movies
                        .Include(movie => movie.User)
                        .Include(movie => movie.MovieUserRatings)
                        .Where(movie =>
                            string.Equals(movie.User.Id, userId, StringComparison.InvariantCultureIgnoreCase))
                        .OrderByDescending(movie => movie.DateOfPublication)
                        .ToListAsync();
                    break;
                default:
                    movies = await _context.Movies
                        .Include(movie => movie.User)
                        .Include(movie => movie.MovieUserRatings)
                        .Where(movie =>
                            string.Equals(movie.User.Id, userId, StringComparison.InvariantCultureIgnoreCase))
                        .ToListAsync();
                    break;

            }

            return movies;
        }

        public async Task<Movie> FindMovieByIdAsync(int id)
        {
            return await _context.Movies.FirstOrDefaultAsync(m => m.MovieID == id);
        }

        public async Task UpdateMovieAsync(Movie movie)
        {
            _context.Update(movie);
            await _context.SaveChangesAsync();
        }

        public async Task<int> InsertMovieAsync(Movie movie)
        {
            _context.Add(movie);
            return await _context.SaveChangesAsync();
        }
    }
}
