﻿using System.Threading.Tasks;
using MovieRama.Web.Entities;

namespace MovieRama.Web.Domain
{
    public interface IMovieUserRatingsRepository
    {
        Task<MovieUserRating> FindMovieUserRatingByMovieIdAndUserIdAsync(string userId, int movieId);

        Task<int> InsertMovieUserRatingAsync(MovieUserRating movieUserRating);

        Task<int> DeleteMovieUserRatingAsync(MovieUserRating movieUserRating);

        Task<int> CountMovieLikesAsync(int movieId);

        Task<int> CountMovieHatesAsync(int movieId);
    }
}
