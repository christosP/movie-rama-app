﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MovieRama.Web.Entities;

namespace MovieRama.Web.Domain
{
    public interface IMoviesRepository
    {
        Task<List<Movie>> GetMoviesOrderedByAsync(string orderBy);

        Task<List<Movie>> GetMoviesPostedByUserOrderedByAsync(string userId, string orderBy);

        Task<Movie> FindMovieByIdAsync(int id);

        Task UpdateMovieAsync(Movie movie);

        Task<int> InsertMovieAsync(Movie movie);
    }
}
