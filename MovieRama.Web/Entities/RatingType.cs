﻿using System.ComponentModel.DataAnnotations.Schema;

namespace MovieRama.Web.Entities
{
    public enum RatingTypes
    {
        Like = 1,
        Hate = 2
    }

    public class RatingType
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RatingTypeID { get; set; }

        public string Name { get; set; }
    }
}
