﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MovieRama.Web.Models;

namespace MovieRama.Web.Entities
{
    public class MovieUserRating
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MovieUserRatingID { get; set; }

        [Required]
        public int MovieId { get; set; }

        [ForeignKey("MovieId")]
        public virtual Movie Movie { get; set; }

        [Required]
        public string UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }

        [Required]
        public int RatingId { get; set; }

        [ForeignKey("RatingId")]
        public virtual RatingType RatingType { get; set; }

        public MovieUserRating() { }

        public MovieUserRating(int movieId, string userId, int ratingId)
        {
            MovieId = movieId;
            UserId = userId;
            RatingId = ratingId;
        }
    }
}
