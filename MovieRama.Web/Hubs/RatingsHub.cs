﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using MovieRama.Web.Domain;
using MovieRama.Web.Entities;

namespace MovieRama.Web.Hubs
{
    public class RatingHub : Hub
    {

        private readonly IMoviesRepository _moviesRepository;
        private readonly IMovieUserRatingsRepository _movieUserRatingsRepository;

        public RatingHub(IMoviesRepository moviesRepository, IMovieUserRatingsRepository movieUserRatingsRepository)
        {
            _moviesRepository = moviesRepository;
            _movieUserRatingsRepository = movieUserRatingsRepository;
        }

        [Authorize]
        public async Task Like(int movieId)
        {
            var movie = await _moviesRepository.FindMovieByIdAsync(movieId);

            if (movie == null) // Verify that like vote send for a movie that exists.
            {
                await Clients.Caller.SendAsync("LikeResponseFailure", "Movie not found.");
                return;
            }

            var contextUserId = Context.User.FindFirstValue(ClaimTypes.NameIdentifier);
            if (movie.UserId == contextUserId) // Verify that user that send the like vote is not the one that posted the movie.
            {
                await Clients.Caller.SendAsync("LikeResponseFailure", "You cannot vote for a movie you have posted.");
                return;
            }

            var hateVote = await _movieUserRatingsRepository.FindMovieUserRatingByMovieIdAndUserIdAsync(contextUserId, movieId);
            if (hateVote != null) // User changed its mind and liked the movie, while previously has hated it. 
            {
                await _movieUserRatingsRepository.DeleteMovieUserRatingAsync(hateVote);

                var hates = await _movieUserRatingsRepository.CountMovieHatesAsync(movieId);

                await Clients.All.SendAsync("HateResponseSuccess", movieId, hates);
            }

            var movieUserRating = new MovieUserRating(movieId, contextUserId, (int) RatingTypes.Like);

            await _movieUserRatingsRepository.InsertMovieUserRatingAsync(movieUserRating);

            var likes = await _movieUserRatingsRepository.CountMovieLikesAsync(movieId);

            await Clients.All.SendAsync("LikeResponseSuccess", movieId, likes);
        }

        [Authorize]
        public async Task Hate(int movieId)
        {
            var movie = await _moviesRepository.FindMovieByIdAsync(movieId);

            if (movie == null) // Verify that hate vote send for a movie that exists.
            {
                await Clients.Caller.SendAsync("HateResponseFailure", "Movie not found.");
                return;
            }

            var contextUserId = Context.User.FindFirstValue(ClaimTypes.NameIdentifier);
            if (movie.UserId == contextUserId) // Verify that user that send the like vote is not the one that posted the movie.
            {
                await Clients.Caller.SendAsync("HateResponseFailure", "You cannot vote for a movie you have posted.");
                return;
            }

            var likeVote = await _movieUserRatingsRepository.FindMovieUserRatingByMovieIdAndUserIdAsync(contextUserId, movieId);
            if (likeVote != null) // User changed its mind and hated the movie, while previously has liked it. 
            {
                await _movieUserRatingsRepository.DeleteMovieUserRatingAsync(likeVote);

                var likes = await _movieUserRatingsRepository.CountMovieLikesAsync(movieId);

                await Clients.All.SendAsync("LikeResponseSuccess", movieId, likes);
            }

            var movieUserRating = new MovieUserRating(movieId, contextUserId, (int) RatingTypes.Hate);

            await _movieUserRatingsRepository.InsertMovieUserRatingAsync(movieUserRating);

            var hates = await _movieUserRatingsRepository.CountMovieHatesAsync(movieId);

            await Clients.All.SendAsync("HateResponseSuccess", movieId, hates);
        }

        [Authorize]
        public async Task RetractVote(int movieId)
        {
            var movie = await _moviesRepository.FindMovieByIdAsync(movieId);

            if (movie == null) // You can't retract a vote for a movie that does not exist.
            {
                await Clients.Caller.SendAsync("RetractVoteFailure", "Movie not found.");
                return;
            }

            var contextUserId = Context.User.FindFirstValue(ClaimTypes.NameIdentifier);

            var movieUserRating = await _movieUserRatingsRepository.FindMovieUserRatingByMovieIdAndUserIdAsync(contextUserId, movieId);
            if (movieUserRating == null) // Use has voted at all.
            {
                await Clients.Caller.SendAsync("RetractVoteFailure", "You haven't voted at all.");
                return;
            }

            // Retract users vote
            await _movieUserRatingsRepository.DeleteMovieUserRatingAsync(movieUserRating);

            // Update either likes/hates correspondigly.
            if (movieUserRating.RatingId == (int) RatingTypes.Like)
            {
                var likes = await _movieUserRatingsRepository.CountMovieLikesAsync(movieId);
                await Clients.All.SendAsync("RetractVoteSuccess", movieId, true, false, likes);
            }

            if (movieUserRating.RatingId == (int) RatingTypes.Hate)
            {
                var hates = await _movieUserRatingsRepository.CountMovieHatesAsync(movieId);
                await Clients.All.SendAsync("RetractVoteSuccess", movieId, false, true, hates);
            }
        }
    }
}
