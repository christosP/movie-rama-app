﻿$(function() {
    var connection = new signalR.HubConnectionBuilder().withUrl("/ratingHub").build();

    connection.on("LikeResponseSuccess",
        function (movieId, likes) {
            var movieLikeBtnSpan = $("div[data-movieid='" + movieId + "'] > .js-likeBtn > span");
            movieLikeBtnSpan.text(" " + likes);
        });

    connection.on("LikeResponseFailure",
        function (error) {
            alert(error);
        });

    connection.on("HateResponseSuccess",
        function (movieId, hates) {
            var movieHateBtn = $("div[data-movieid='" + movieId + "'] > .js-hateBtn > span");
            movieHateBtn.text(" " + hates);
        });

    connection.on("HateResponseFailure",
        function(error) {
            alert(error);
        });

    connection.on("RetractVoteSuccess",
        function (movieId, likeVoteRetracted, hateVoteRetracted, value) {
            if (likeVoteRetracted) {
                var movieLikeBtnSpan = $("div[data-movieid='" + movieId + "'] > .js-likeBtn > span");
                movieLikeBtnSpan.text(" " + value);

                var movieLikeBtn = $("div[data-movieid='" + movieId + "'] > .js-likeBtn");
                movieLikeBtn.removeAttr("disabled");
            }

            if (hateVoteRetracted) {
                var movieHateBtnSpan = $("div[data-movieid='" + movieId + "'] > .js-hateBtn > span");
                movieHateBtnSpan.text(" " + value);

                var movieHateBtn = $("div[data-movieid='" + movieId + "'] > .js-hateBtn");
                movieHateBtn.removeAttr("disabled");

            }
        });

    connection.on("RetractVoteFailure",
        function (error) {
            alert(error);
        });


    connection.start()
        .then(function () {
            console.log("connection started");
        }).catch(function (err) {
            return console.error(err.toString());
        });

    registerBtnClickHandler(".js-likeBtn", ".js-hateBtn", "Like");
    registerBtnClickHandler(".js-hateBtn", ".js-likeBtn", "Hate");

    function registerBtnClickHandler(opinionBtnClassName, oppositeOpinionBtnClassName, hubMethod) {
        $(opinionBtnClassName).on("click",
            function () {
                var movieId = $(this).parent().data("movieid");
                connection.invoke(hubMethod, movieId).catch(function (err) {
                    return console.error(err.toString());
                });

                var closestOppositeOpinionBtn = $(this).siblings(oppositeOpinionBtnClassName);
                if (closestOppositeOpinionBtn.prop("disabled")) {
                    closestOppositeOpinionBtn.removeAttr("disabled");
                }

                var retractBtn = $(this).siblings(".js-retractVote");
                if (retractBtn.prop("disabled")) {
                    retractBtn.removeAttr("disabled");
                }

                $(this).attr("disabled", "disabled");
            });
    }

    $(".js-retractVote").on("click",
        function() {
            var movieId = $(this).parent().data("movieid");
            connection.invoke("RetractVote", movieId).catch(function (err) {
                return console.error(err.toString());
            });

            $(this).attr("disabled", "disabled");
        });
});