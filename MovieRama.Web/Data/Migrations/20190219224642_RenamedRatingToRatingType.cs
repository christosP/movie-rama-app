﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MovieRama.Web.Data.Migrations
{
    public partial class RenamedRatingToRatingType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MovieUserRatings_Ratings_RatingId",
                table: "MovieUserRatings");

            migrationBuilder.DropTable(
                name: "Ratings");

            migrationBuilder.CreateTable(
                name: "RatingTypes",
                columns: table => new
                {
                    RatingTypeID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RatingTypes", x => x.RatingTypeID);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_MovieUserRatings_RatingTypes_RatingId",
                table: "MovieUserRatings",
                column: "RatingId",
                principalTable: "RatingTypes",
                principalColumn: "RatingTypeID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MovieUserRatings_RatingTypes_RatingId",
                table: "MovieUserRatings");

            migrationBuilder.DropTable(
                name: "RatingTypes");

            migrationBuilder.CreateTable(
                name: "Ratings",
                columns: table => new
                {
                    RatingID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ratings", x => x.RatingID);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_MovieUserRatings_Ratings_RatingId",
                table: "MovieUserRatings",
                column: "RatingId",
                principalTable: "Ratings",
                principalColumn: "RatingID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
