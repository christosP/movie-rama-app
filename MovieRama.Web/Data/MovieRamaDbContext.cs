﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MovieRama.Web.Entities;
using MovieRama.Web.Models;

namespace MovieRama.Web.Data
{
    public class MovieRamaDbContext : IdentityDbContext<ApplicationUser>
    {
        public MovieRamaDbContext(DbContextOptions<MovieRamaDbContext> options)
            : base(options)
        {
        }

        public DbSet<Movie> Movies { get; set; }
        public DbSet<MovieUserRating> MovieUserRatings { get; set; }
        public DbSet<RatingType> RatingTypes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<MovieUserRating>()
                .HasAlternateKey(mur => new {mur.MovieId, mur.UserId, mur.RatingId});
        }
    }
}
