﻿namespace MovieRama.Web.Areas.Identity.Services
{
    /// <summary>
    /// https://docs.microsoft.com/en-us/aspnet/core/security/authentication/accconfirm
    /// </summary>
    public class AuthMessageSenderOptions
    {
        public string SendGridUser { get; set; }
        public string SendGridKey { get; set; }
    }
}
