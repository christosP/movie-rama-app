﻿using Microsoft.AspNetCore.Hosting;

[assembly: HostingStartup(typeof(MovieRama.Web.Areas.Identity.IdentityHostingStartup))]
namespace MovieRama.Web.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
                
            });
        }
    }
}