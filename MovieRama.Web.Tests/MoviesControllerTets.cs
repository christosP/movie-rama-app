﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using MovieRama.Web.Controllers;
using MovieRama.Web.Domain;
using MovieRama.Web.Entities;
using MovieRama.Web.Models;
using NUnit.Framework;

namespace MovieRama.Web.Tests
{
    [TestFixture]
    public class MoviesControllerTests
    {
        private MockRepository _mockRepository;

        [SetUp]
        public void Setup()
        {
            _mockRepository = new MockRepository(MockBehavior.Strict);
        }

        [TearDown]
        public void TearDown() => _mockRepository.VerifyAll();

        [Test]
        public async Task Get_EditMovie_ReturnsNotFound_WhenMovieDoesNotExist()
        {
            // Arrange
            var moviesRepositoryMock = _mockRepository.Create<IMoviesRepository>();

            moviesRepositoryMock.Setup(x => x.FindMovieByIdAsync(1)).Returns(Task.FromResult<Movie>(null));

            var moviesController = new MoviesController(moviesRepositoryMock.Object);

            // Act
            var result = await moviesController.Edit(1);

            // Assert
            Assert.IsNotNull(result as NotFoundResult);
        }

        [Test]
        public async Task Get_EditMovie_ReturnsUnauthorized_WhenEditIsRequestedByAnyoneElseThanTheOnePostedIt()
        {
            // Arrange
            var moviesRepositoryMock = _mockRepository.Create<IMoviesRepository>();

            var movie = new Movie
            {
                UserId = "2"
            };

            moviesRepositoryMock.Setup(x => x.FindMovieByIdAsync(1)).Returns(Task.FromResult(movie));

            var moviesController = CreateMoviesControllerWithUserId(moviesRepositoryMock.Object, userId: "1");

            // Act 
            var result = await moviesController.Edit(1);

            // Assert
            Assert.IsNotNull(result as UnauthorizedResult);
        }

        [Test]
        public async Task Post_EditMovie_ReturnsNotFound_WhenPostedMovieIdDiffersFromIdInUrl()
        {
            // Arrange
            var moviesRepositoryMock = _mockRepository.Create<IMoviesRepository>();

            var moviesController = new MoviesController(moviesRepositoryMock.Object);

            // Act 
            var result = await moviesController.Edit(1, new MovieViewModel {MovieId = 2});

            // Assert
            Assert.IsNotNull(result as NotFoundResult);
        }

        [Test]
        public async Task Post_EditMovie_ReturnsUnauthorized_WhenEditIsRequestedByAnyoneElseThanTheOnePostedIt()
        {
            // Arrange
            var movie = new Movie
            {
                UserId = "2"
            };

            var moviesRepositoryMock = _mockRepository.Create<IMoviesRepository>();

            moviesRepositoryMock.Setup(x => x.FindMovieByIdAsync(2)).Returns(Task.FromResult(movie));

            var moviesController = CreateMoviesControllerWithUserId(moviesRepositoryMock.Object, userId: "1");

            // Act 
            var result = await moviesController.Edit(2, new MovieViewModel {MovieId = 2});

            // Assert
            Assert.IsNotNull(result as UnauthorizedResult);
        }

        [Test]
        public async Task Post_EditMovie_WhenIsSuccessfull_RedirectsToTheIndex()
        {
            // Arrange
            var movie = new Movie
            {
                UserId = "2"
            };

            var moviesRepositoryMock = _mockRepository.Create<IMoviesRepository>();

            moviesRepositoryMock.Setup(x => x.FindMovieByIdAsync(2)).Returns(Task.FromResult(movie));
            moviesRepositoryMock.Setup(x => x.UpdateMovieAsync(movie)).Returns(Task.CompletedTask);

            var moviesController = CreateMoviesControllerWithUserId(moviesRepositoryMock.Object, userId: "2");

            // Act 
            var result = await moviesController.Edit(2, new MovieViewModel { MovieId = 2 });


            // Assert
            var redirectionResult = result as RedirectToActionResult;
            Assert.IsNotNull(redirectionResult);
            Assert.IsTrue(redirectionResult.ControllerName == null && redirectionResult.ActionName == "Index");
        }

        [Test]
        public async Task Post_EditMovie_WhenModelIsNotValid_ReturnViewWithErrors()
        {
            // Arrange
            var movie = new Movie
            {
                UserId = "2"
            };

            var moviesRepositoryMock = _mockRepository.Create<IMoviesRepository>();

            moviesRepositoryMock.Setup(x => x.FindMovieByIdAsync(2)).Returns(Task.FromResult(movie));

            var moviesController = CreateMoviesControllerWithUserId(moviesRepositoryMock.Object, userId: "2");

            moviesController.ModelState.AddModelError("key", "error");

            // Act 
            var result = await moviesController.Edit(2, new MovieViewModel { MovieId = 2 });

            // Assert
            var viewResult = result as ViewResult;
            Assert.IsNotNull(viewResult);
        }

        private static MoviesController CreateMoviesControllerWithUserId(IMoviesRepository moviesRepository, string userId)
        {
            return new MoviesController(moviesRepository)
            {
                ControllerContext = new ControllerContext
                {
                    HttpContext = new DefaultHttpContext
                    {
                        User = new ClaimsPrincipal(new ClaimsIdentity(new[]
                        {
                            new Claim(ClaimTypes.NameIdentifier, userId)
                        }))
                    }
                }
            };
        }
    }
}
